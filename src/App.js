import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import ActionForm from './component/ActionForm';

function App() {
  return (
    <div className="container">
        <ActionForm/>
    </div>
  );
}

export default App;
