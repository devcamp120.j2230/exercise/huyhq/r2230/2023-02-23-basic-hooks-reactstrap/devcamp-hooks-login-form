import { useState } from "react";
import { Input, Col, Form, FormGroup, Row, Button } from "reactstrap";

const link = {
    textDecoration: "none",
    color: "green"
}

const LoginForm = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const emailOnChange = (e) => {
        let value = e.target.value;
        setEmail(value);
    }

    const passwordOnChange = (e) => {
        let value = e.target.value;
        setPassword(value);
    }

    const loginForm = () => {
        if(email ==""){
            alert("Email không được để trống.");
            return;
        };

        if(password ==""){
            alert("Password không được để trống.");
            return;
        };

        console.log("Login Success!");
        console.log("Email: " + email);
        console.log("Password: " + password);
    };

    return (
        <Col className="text-center">
            <Col className="text-center mt-5">
                <h3>Welcome Back!</h3>
            </Col>
            <Form className="mt-5">
                <FormGroup>
                    <Input type="text" placeholder="Email *" value={email} onChange={emailOnChange} />
                </FormGroup>
                <FormGroup>
                    <Input type="password" placeholder="Password *" value={password} onChange={passwordOnChange} />
                </FormGroup>
            </Form>
            <Col className="text-end"><a href="#" style={link}>Forgot password?</a></Col>
            <Col className="row mt-5"><Button color="success" onClick={loginForm}>Login</Button></Col>
        </Col>
    )
}

export default LoginForm;