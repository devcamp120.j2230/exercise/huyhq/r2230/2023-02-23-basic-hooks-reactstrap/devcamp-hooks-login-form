import { useState } from "react";
import { Col, Row} from "reactstrap";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";

const cursorPointer = {
    cursor: "pointer"
}

const ActionForm = () => {
    const [status, setStatus] = useState("login");

    const clickGetSignupForm = () => {
        setStatus("signup");
    };

    const clickGetLoginForm = () => {
        setStatus("login");
    };

    return (
        <Col className="text-center">
            <Col md="6" sm="12" className="text-center my-5">
                <Row className="text-center">
                    <Col className="row">
                        <div className={status == "login" ? "bg-secondary p-3" : "bg-info p-3"}
                            style={cursorPointer}
                            onClick={clickGetSignupForm}>Sign Up</div>
                    </Col>
                    <Col className="row">
                        <div className={status == "signup" ? "bg-secondary p-3" : "bg-info p-3"}
                            style={cursorPointer}
                            onClick={clickGetLoginForm}>Login</div>
                    </Col>
                </Row>

                {status == "login" ? <LoginForm /> : <RegisterForm />}
            </Col>
        </Col>
    )
}

export default ActionForm